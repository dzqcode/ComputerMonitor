﻿using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;

namespace Github;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("-----------获取Github的DNS记录-----------");

        HttpClient client = new();
        List<string> list = ["180.76.76.76", "223.5.5.5", "223.6.6.6", "119.29.29.29", "114.114.114.114", "8.8.8.8", "208.67.222.222", "216.146.35.35", "8.26.56.26", "156.154.70.1", "101.226.4.6", "1.2.4.8"];
        List<RecordItem> records = [];
        foreach (var ip in list)
        {
            Console.WriteLine($"{ip}");
            var resTask = client.PostAsync("http://api.ip33.com/dns/resolver", new StringContent("domain=github.com&type=A&dns=" + ip, Encoding.UTF8, "application/x-www-form-urlencoded"));
            string ss = resTask.Result.Content.ReadAsStringAsync().Result;
            var root = JsonSerializer.Deserialize<Root>(ss);

            if (root != null)
            {
                if (root.record != null)
                {
                    records.AddRange(root.record);
                }
            }
        }
        records = records.OrderBy(x => x.ttl).ToList();

        for (int i = 0; i < records.Count; i++)
        {
            Console.WriteLine($"{i} : {records[i].ip} - {records[i].ttl}");
        }
        Console.WriteLine("-----------选择ip 序号-----------");
        string no = Console.ReadLine();
        int idx = int.Parse(no);
        Console.WriteLine($"选择IP：{records[idx].ip} ttl值：{records[idx].ttl}");
        Console.WriteLine("确定写入hosts文件？y/n");
        string yn = Console.ReadLine();
        if (yn != "y")
        {
            Console.WriteLine("退出");
            return;
        }
        string tales = $"{records[idx].ip} github.com";
        string hostfile = @"C:\Windows\System32\drivers\etc\hosts";
        string[] lines = File.ReadAllLines(hostfile);

        if (lines.Any(s => s.Contains("github.com")))
        {
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains("github.com"))
                    lines[i] = tales;
            }
            File.WriteAllLines(hostfile, lines);
        }
        else if (!lines.Contains(tales))
        {
            File.AppendAllLines(hostfile, [tales]);
        }
        Console.WriteLine("-----------刷新hosts文件-----------");
        var r = DnsFlushResolverCache();
        System.Diagnostics.Process process = new();
        System.Diagnostics.ProcessStartInfo startInfo = new()
        {
            WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden,
            FileName = "cmd.exe",
            Arguments = "/C ipconfig /flushdns"
        };
        process.StartInfo = startInfo;
        process.Start();
        Console.WriteLine($"-----------刷新完成{r}-----------");
        Console.WriteLine("按任意键退出...");
        Console.ReadKey();
    }

    [DllImport("Dnsapi.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool DnsFlushResolverCache();
}


public class RecordItem
{
    /// <summary>
    /// 
    /// </summary>
    public string ip { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public int ttl { get; set; }
}

public class Root
{
    /// <summary>
    /// 
    /// </summary>
    public string dns { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string type { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public string domain { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public bool state { get; set; }
    /// <summary>
    /// 
    /// </summary>
    public List<RecordItem> record { get; set; }
}