﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComputerMonitor
{
    public partial class FormOcr : Form
    {
        public FormOcr(string txt)
        {
            InitializeComponent();
            TextBoxOcr.Text = txt;
        }
    }
}
