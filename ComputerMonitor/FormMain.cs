using System.Configuration;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.Management;
using System.Runtime.InteropServices;
using System.Text.Json;
using System.Timers;

namespace ComputerMonitor
{
    public partial class FormMain : Form
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        extern static bool DestroyIcon(IntPtr handle);

        readonly List<double> cpus = [];
        readonly List<double> rams = [];

        // 用于获得CPU、RAM信息
        readonly PerformanceCounter cpuUsage;
        readonly PerformanceCounter ramCounter;
        //总内存
        readonly long totalRam = 1;
        //颜色字典
        readonly Dictionary<int, Color> colors;

        //热键帮助类
        private readonly HotkeyHelper _hotKey;
        public FormMain()
        {
            InitializeComponent();
            this.FormClosing += FormMain_FormClosing;
            this.NotifyIcon.Icon = Properties.Resources.icon;
            //初始化4个等级的颜色
            colors = new Dictionary<int, Color>() {
                { 0, Color.FromArgb(51, 255, 0) },
                { 1, Color.FromArgb(204, 255, 0) },
                { 2, Color.FromArgb(255, 204, 0) },
                { 3, Color.FromArgb(255, 102, 0) }
            };
            totalRam = GetTotalRAM();
            // 初始化计数器
            cpuUsage = new PerformanceCounter("Processor Information", "% Processor Utility", "_Total");
            ramCounter = new PerformanceCounter("Memory", "Available MBytes");
            System.Timers.Timer timer = new(2000);
            timer.Elapsed += Timer_Tick;
            timer.AutoReset = true; ;
            timer.Enabled = true;
            // 注册热键
            _hotKey = new HotkeyHelper(this.Handle);
            _hotKey.Register();
        }

        private void FormMain_FormClosing(object? sender, FormClosingEventArgs e)
        {
            _hotKey?.UnRegister();
        }

        private void Timer_Tick(object? sender, ElapsedEventArgs e)
        {
            int cpu = (int)Avg(cpus, cpuUsage.NextValue());
            if (cpu >= 100)
            {
                //3位数显示不好看
                cpu = 99;
            }
            float ram = (float)Math.Round(Avg(rams, ((totalRam - ramCounter.NextValue()) / totalRam) * 100));

            NotifyIcon.Text = $"CPU：{cpu}%\r\nRAM：{ram}%";
            //绘制icon，颜色代表内存使用率，数字是cpu使用率
            Bitmap icon = new(32, 32);//创建图标
            using (Graphics g = Graphics.FromImage(icon))//获取图标绘图表面
            {
                g.Clear(Color.Transparent);
                g.SmoothingMode = SmoothingMode.AntiAlias;//开启消除锯齿
                g.DrawString(cpu.ToString("D2"), new Font("DS-Digital", 34), new SolidBrush(GetColor(ram)), -9, 0);
                IntPtr ptr = icon.GetHicon();
                NotifyIcon.Icon.Dispose();
                NotifyIcon.Icon = Icon.FromHandle(ptr);
                DestroyIcon(ptr); //释放icon句柄，不然长时间运行会挂掉
            }
            icon.Dispose();
        }
        //重写消息循环
        protected override void WndProc(ref Message m)
        {
            const int WM_HOTKEY = 0x0312;
            if (m.Msg == WM_HOTKEY && m.WParam.ToInt64() == _hotKey.screenkeyID) //判断热键
            {
                //调用截图工具 https://gitee.com/horsejs_admin/ScreenCapture
                Process.Start("ScreenCapture.exe");
            }
            else if (m.Msg == WM_HOTKEY && m.WParam.ToInt64() == _hotKey.gitKeyID)
            {
                Process.Start("Github.exe");
            }
            else if (_hotKey != null && m.WParam.ToInt64() == _hotKey.ocrkeyID)
            {
                //new ColorPicker().ShowDialog();
                //从剪切板中获取图片
                try
                {
                    IDataObject iData = Clipboard.GetDataObject();
                    if (iData.GetDataPresent(DataFormats.Bitmap))
                    {
                        var img = Clipboard.GetImage();
                        string log = string.Empty;

                        //获取token,同步方式调用
                        var API_KEY = ConfigurationManager.AppSettings["OCR_API_KEY"];
                        var SECRET_KEY = ConfigurationManager.AppSettings["OCR_SECRET_KEY"];
                        var client = new HttpClient();
                        var dic = new Dictionary<string, string>
                        {
                            { "client_id", API_KEY },
                            { "client_secret", SECRET_KEY },
                            {"grant_type","client_credentials" }
                        };
                        var response = client.PostAsync("https://aip.baidubce.com/oauth/2.0/token", new FormUrlEncodedContent(dic)).Result;
                        var res = response.Content.ReadAsStringAsync().Result;
                        log = res;
                        var token = JsonSerializer.Deserialize<AccessToken>(res);

                        MemoryStream mstream = new();
                        img.Save(mstream, System.Drawing.Imaging.ImageFormat.Jpeg);
                        dic = new Dictionary<string, string>
                        {
                            { "image", Convert.ToBase64String(mstream.ToArray()) },
                            {"access_token" ,token.access_token}
                        };
                        response = client.PostAsync("https://aip.baidubce.com/rest/2.0/ocr/v1/accurate_basic", new FormUrlEncodedContent(dic)).Result;
                        res = response.Content.ReadAsStringAsync().Result;
                        log = res;
                        var result = JsonSerializer.Deserialize<WordsResult>(res);
                        string str = "";
                        try
                        {
                            foreach (var item in result.words_result)
                            {
                                str += item.words + "\r\n";
                            }
                        }
                        catch
                        {
                            str = result.ToString();
                        }
                        new FormOcr(str).Show();
                        mstream.Dispose();
                        img.Dispose();
                        client.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    new FormOcr(ex.Message + "\r\n" + ex.StackTrace).Show();
                }
            }
            else if (_hotKey != null && m.WParam.ToInt64() == _hotKey.noteKeyID)
            {
                Process.Start("notepad.exe", @"C:\Users\Administrator\Work\Repositories\linux.txt");
            }
            base.WndProc(ref m);
        }
        readonly ManagementClass managementClass = new("Win32_ComputerSystem");
        const int MbDiv = 1024 * 1024;
        //获取物理总内存
        private long GetTotalRAM()
        {
            //获得物理内存
            long PhysicalMemory = 1;
            var managementObjectCollection = managementClass.GetInstances();
            foreach (var managementBaseObject in managementObjectCollection)
            {
                if (managementBaseObject["TotalPhysicalMemory"] != null)
                {
                    PhysicalMemory = long.Parse(managementBaseObject["TotalPhysicalMemory"].ToString()) / MbDiv;
                    break;
                }
            }
            return PhysicalMemory;
        }

        //近3次数据平均一下，平滑
        private static double Avg(List<double> list, double one)
        {
            if (list.Count >= 3)
            {
                list.RemoveAt(0);
            }
            list.Add(one);
            return list.Sum() / list.Count;
        }

        /// <summary>
        /// 获取颜色，4个等级
        /// </summary>
        /// <param name="percent"></param>
        /// <returns></returns>
        private Color GetColor(float percent)
        {
            if (percent <= 50)
            {
                return colors[0];
            }
            else if (percent < 65)
            {
                return colors[1];
            }
            else if (percent < 80)
            {
                return colors[2];
            }
            return colors[3];
        }
    }
}
