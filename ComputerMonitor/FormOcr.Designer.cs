﻿namespace ComputerMonitor
{
    partial class FormOcr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            TextBoxOcr = new TextBox();
            SuspendLayout();
            // 
            // TextBoxOcr
            // 
            TextBoxOcr.BorderStyle = BorderStyle.None;
            TextBoxOcr.Dock = DockStyle.Fill;
            TextBoxOcr.Font = new Font("Microsoft YaHei UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            TextBoxOcr.Location = new Point(0, 0);
            TextBoxOcr.Multiline = true;
            TextBoxOcr.Name = "TextBoxOcr";
            TextBoxOcr.Size = new Size(800, 450);
            TextBoxOcr.TabIndex = 0;
            // 
            // FormOcr
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(TextBoxOcr);
            Name = "FormOcr";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "OCR";
            TopMost = true;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox TextBoxOcr;
    }
}