﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ComputerMonitor
{
    /// <summary>
    /// 注册全局热键
    /// </summary>
    public class HotkeyHelper
    {
        readonly IntPtr hWnd;
        public int screenkeyID;
        public int ocrkeyID;

        public int noteKeyID;
        public int gitKeyID;

        [DllImport("user32.dll")]
        public static extern uint RegisterHotKey(IntPtr hWnd, uint id, uint fsModifiers, uint vk);

        [DllImport("user32.dll")]
        public static extern uint UnregisterHotKey(IntPtr hWnd, uint id);

        [DllImport("kernel32.dll")]
        public static extern uint GlobalAddAtom(String lpString);

        [DllImport("kernel32.dll")]
        public static extern uint GlobalDeleteAtom(uint nAtom);

        public enum KeyFlags
        {
            NONE = 0x0,
            ALT = 0x1,
            CONTROL = 0x2,
            SHIFT = 0x4,
            WIN = 0x8
        }
        /// <summary>
        /// 自定义按键枚举
        /// </summary>
        public enum EKey
        {
            Space = 32,
            Left = 37,
            Up = 38,
            Right = 39,
            Down = 40,
            A = 65,
            B = 66,
            C = 67,
            D = 68,
            E = 69,
            F = 70,
            G = 71,
            H = 72,
            I = 73,
            J = 74,
            K = 75,
            L = 76,
            M = 77,
            N = 78,
            O = 79,
            P = 80,
            Q = 81,
            R = 82,
            S = 83,
            T = 84,
            U = 85,
            V = 86,
            W = 87,
            X = 88,
            Y = 89,
            Z = 90,
            F1 = 112,
            F2 = 113,
            F3 = 114,
            F4 = 115,
            F5 = 116,
            F6 = 117,
            F7 = 118,
            F8 = 119,
            F9 = 120,
            F10 = 121,
            F11 = 122,
            F12 = 123,
        }
        public HotkeyHelper(IntPtr hWnd)
        {
            this.hWnd = hWnd;
        }

        /// <summary>
        /// 注册全局热键
        /// </summary>
        /// <param name="key"></param>
        /// <param name="keyFlags"></param>
        /// <returns></returns>
        public void Register()
        {
            screenkeyID = (int)GlobalAddAtom(Guid.NewGuid().ToString());
            ocrkeyID = (int)GlobalAddAtom(Guid.NewGuid().ToString());
            noteKeyID = (int)GlobalAddAtom(Guid.NewGuid().ToString());
            gitKeyID = (int)GlobalAddAtom(Guid.NewGuid().ToString());
            var compose = KeyFlags.CONTROL | KeyFlags.ALT; //组合键
            _ = RegisterHotKey(hWnd, (uint)screenkeyID, (uint)compose, (uint)EKey.S); //ctrl+alt+s 截屏
            _ = RegisterHotKey(hWnd, (uint)ocrkeyID, (uint)compose, (uint)EKey.C);  //ctrl+alt+c ocr
            _ = RegisterHotKey(hWnd, (uint)noteKeyID, (uint)compose, (uint)EKey.A);  //ctrl+alt+a 记事本
            _ = RegisterHotKey(hWnd, (uint)gitKeyID, (uint)compose, (uint)EKey.G);  //ctrl+alt+g github加速
        }

        /// <summary>
        /// 注销全局热键
        /// </summary>
        /// <param name="hotkeyID"></param>
        public void UnRegister()
        {
            _ = UnregisterHotKey(hWnd, (uint)screenkeyID);
            _ = UnregisterHotKey(hWnd, (uint)ocrkeyID);
            _ = UnregisterHotKey(hWnd, (uint)noteKeyID);
            _ = UnregisterHotKey(hWnd, (uint)gitKeyID);
            _ = GlobalDeleteAtom((uint)screenkeyID);
            _ = GlobalDeleteAtom((uint)ocrkeyID);
            _ = GlobalDeleteAtom((uint)noteKeyID);
            _ = GlobalDeleteAtom((uint)gitKeyID);
        }
    }
}
