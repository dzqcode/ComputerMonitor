﻿namespace ComputerMonitor
{
    partial class FormMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            NotifyIcon = new NotifyIcon(components);
            SuspendLayout();
            // 
            // NotifyIcon
            // 
            NotifyIcon.Text = "notifyIcon1";
            NotifyIcon.Visible = true;
            // 
            // FormMain
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(120, 0);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormMain";
            Opacity = 0D;
            ShowInTaskbar = false;
            Text = "CPU工具";
            ResumeLayout(false);
        }

        #endregion

        private NotifyIcon NotifyIcon;
    }
}
