﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputerMonitor
{
    public class AccessToken
    {
        /// <summary>
        /// 该参数忽略
        /// </summary>
        public string refresh_token { get; set; }
        /// <summary>
        /// Access Token的有效期(秒)
        /// </summary>
        public int expires_in { get; set; }
        /// <summary>
        /// 该参数忽略
        /// </summary>
        public string session_key { get; set; }
        /// <summary>
        /// 要获取的Access Token
        /// </summary>
        public string access_token { get; set; }
        /// <summary>
        /// 该参数忽略
        /// </summary>
        public string scope { get; set; }
        /// <summary>
        /// 该参数忽略
        /// </summary>
        public string session_secret { get; set; }
    }

    public class WordsResult
    {
        /// <summary>
        /// 识别结果数组
        /// </summary>
        public List<Words_resultItem> words_result { get; set; }
        /// <summary>
        /// 识别结果数，表示words_result的元素个数
        /// </summary>
        public int words_result_num { get; set; }
        /// <summary>
        /// 唯一的log id，用于问题定位
        /// </summary>
        public long log_id { get; set; }
    }

    public class Words_resultItem
    {
        /// <summary>
        /// 文件传输助手
        /// </summary>
        public string words { get; set; }
    }
}
