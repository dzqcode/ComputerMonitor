# ComputerMonitor

#### 介绍
cpu内存监控

安装DS-Digital.ttf字体（字体文件很小，仅包含0-9）

运行效果：
![输入图片说明](https://foruda.gitee.com/images/1708221993197823734/fe852c1d_348286.png "屏幕截图")

数字代表cpu占用率，颜色代表内存占用率，还包括了一些截图等小功能，自己根据需要二次开发。